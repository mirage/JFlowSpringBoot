package BP.Frm;

import java.util.ArrayList;
import java.util.List;

import BP.En.EntitiesMyPK;
import BP.En.Entity;

/** 
 连接方法
*/
public class MethodLinks extends EntitiesMyPK
{
	/** 
	 连接方法
	*/
	public MethodLinks()
	{
	}
	/** 
	 得到它的 Entity 
	*/
	@Override
	public Entity getNewEntity()
	{
		return new MethodLink();
	}

		///#region 为了适应自动翻译成java的需要,把实体转换成List.
	/** 
	 转化成 java list,C#不能调用.
	 
	 @return List
	*/
	public final List<MethodLink> ToJavaList()
	{
		return (List<MethodLink>)(Object)this;
	}
	/** 
	 转化成list
	 
	 @return List
	*/
	public final ArrayList<MethodLink> Tolist()
	{
		ArrayList<MethodLink> list = new ArrayList<MethodLink>();
		for (int i = 0; i < this.size(); i++)
		{
			list.add((MethodLink)this.get(i));
		}
		return list;
	}

		///#endregion 为了适应自动翻译成java的需要,把实体转换成List.
}