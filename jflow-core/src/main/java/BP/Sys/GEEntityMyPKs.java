package BP.Sys;
import java.util.ArrayList;
import java.util.List;

import BP.En.Entities;
import BP.En.Entity;

/** 
 通用OID实体s
*/
public class GEEntityMyPKs extends Entities
{

		///#region 重载基类方法
	@Override
	public String toString()
	{
		return this.FK_MapData;
	}
	/** 
	 主键
	*/
	public String FK_MapData = null;

		///#endregion


		///#region 方法
	/** 
	 得到它的 Entity
	*/
	@Override
	public Entity getNewEntity()
	{
		if (this.FK_MapData == null)
		{
			return new GEEntityMyPK();
		}
		return new GEEntityMyPK(this.FK_MapData);
	}
	/** 
	 通用OID实体ID
	*/
	public GEEntityMyPKs()
	{
	}
	/** 
	 通用OID实体ID
	 
	 @param fk_mapdtl
	*/
	public GEEntityMyPKs(String fk_mapdata)
	{
		this.FK_MapData = fk_mapdata;
	}

		///#endregion


		///#region 为了适应自动翻译成java的需要,把实体转换成List.
	/** 
	 转化成 java list,C#不能调用.
	 
	 @return List
	*/
	public final List<GEEntityMyPK> ToJavaList()
	{
		return (List<GEEntityMyPK>)(Object)this;
	}
	/** 
	 转化成list
	 
	 @return List
	*/
	public final ArrayList<GEEntityMyPK> Tolist()
	{
		ArrayList<GEEntityMyPK> list = new ArrayList<GEEntityMyPK>();
		for (int i = 0; i < this.size(); i++)
		{
			list.add((GEEntityMyPK)this.get(i));
		}
		return list;
	}

		///#endregion 为了适应自动翻译成java的需要,把实体转换成List.
}