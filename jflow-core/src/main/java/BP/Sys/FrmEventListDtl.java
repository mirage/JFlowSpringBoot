package BP.Sys;

public class FrmEventListDtl
{
	/** 
	 从表保存前
	*/
	public static final String RowSaveBefore = "DtlRowSaveBefore";
	/** 
	 从表保存后
	*/
	public static final String RowSaveAfter = "DtlRowSaveAfter";

	/** 
	 从表保存前
	*/
	public static final String DtlRowDelBefore = "DtlRowDelBefore";
	/** 
	 从表保存后
	*/
	public static final String DtlRowDelAfter = "DtlRowDelAfter";

}