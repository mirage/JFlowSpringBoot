package BP.WF.Template;

/** 
 工具栏属性
*/
public class NodeToolbarAttr extends BP.En.EntityOIDNameAttr
{

		///#region 基本属性
	/** 
	 节点
	*/
	public static final String FK_Node = "FK_Node";
	/** 
	 到达目标
	*/
	public static final String Target = "Target";
	/** 
	 标题
	*/
	public static final String Title = "Title";
	/** 
	 url
	*/
	public static final String Url = "Url";
	/** 
	 顺序号
	*/
	public static final String Idx = "Idx";
	/** 
	 显示在那里？
	*/
	public static final String ShowWhere = "ShowWhere";
	/** 
	 执行类型
	*/
	public static final String ExcType = "ExcType";

		///#endregion
}