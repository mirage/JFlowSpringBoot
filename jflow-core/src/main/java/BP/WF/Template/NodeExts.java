package BP.WF.Template;

import java.util.List;

import BP.En.Entities;
import BP.En.Entity;

/** 
 节点集合
*/
public class NodeExts extends Entities
{

		///#region 构造方法
	/** 
	 节点集合
	*/
	public NodeExts()
	{
	}

	public NodeExts(String fk_flow) throws Exception
	{
		this.Retrieve(NodeAttr.FK_Flow, fk_flow, NodeAttr.Step);
		return;
	}

	public final List<NodeExt> ToJavaList()
	{
		return (List<NodeExt>)(Object)this;
	}

	@Override
	public Entity getNewEntity()
	{
		return new NodeExt();
	}
}