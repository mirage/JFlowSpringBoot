package BP.WF.XML;

import BP.Sys.XML.XmlEnNoName;
import BP.Sys.XML.XmlEns;

/** 
 工作一户式
*/
public class OneWorkXml extends XmlEnNoName
{

		///#region 属性.
	public final String getName()
	{
		return this.GetValStringByKey(BP.Web.WebUser.getSysLang());
	}
	public final String getURL()
	{
		 
			return this.GetValStringByKey("Url"+BP.WF.Glo.Plant);
		 
	}

	public final String getIsDefault()
	{
		return this.GetValStringByKey("IsDefault");
	}



		///#endregion 属性.


		///#region 构造
	/** 
	 节点扩展信息
	*/
	public OneWorkXml()
	{
	}
	/** 
	 获取一个实例
	*/
	@Override
	public XmlEns getGetNewEntities()
	{
		return new OneWorkXmls();
	}

		///#endregion
}