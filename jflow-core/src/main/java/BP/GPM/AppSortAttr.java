package BP.GPM;

import BP.En.EntityNoNameAttr;

/** 
 系统类别
*/
public class AppSortAttr extends EntityNoNameAttr
{
	public static final String Idx = "Idx";
	/** 
	 关联的菜单编号
	*/
	public static final String RefMenuNo = "RefMenuNo";
}