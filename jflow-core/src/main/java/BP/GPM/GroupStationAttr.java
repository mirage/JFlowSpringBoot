package BP.GPM;

/** 
 权限组岗位
*/
public class GroupStationAttr
{
	/** 
	 操作员
	*/
	public static final String FK_Station = "FK_Station";
	/** 
	 权限组
	*/
	public static final String FK_Group = "FK_Group";
}